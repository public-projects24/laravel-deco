// Selectors
let deleteRecord = document.querySelectorAll('.form-delete-record');

// Events
deleteRecord.forEach(element => {
    element.addEventListener('submit', clickDeleteRecord);
});

// Functions
function clickDeleteRecord() {
    event.preventDefault();
    let element = event.target;

    if(confirm('This record will be deleted. Are you sure to continue?')) {
        element.submit();
    }
}

