window.addEventListener("DOMContentLoaded", (event) => {
    // Selectors
    const formRequest = document.querySelector("#form-request-service");
    if (!formRequest) {
        return;
    }

    const nameInput = formRequest.querySelector('[name="name"]');
    const telephoneInput = formRequest.querySelector('[name="telephone"]');
    const personSelect = formRequest.querySelector('[name="person"]');
    const businessNameInput = formRequest.querySelector(
        '[name="business-name"]'
    );

    const containerBusinessName = formRequest.querySelector(
        ".container-business-name"
    );
    const btnNextStep = formRequest.querySelector("#btn-next-step");
    const containerFirstStep = formRequest.querySelector(
        ".steps .step:first-child"
    );
    const containerSecondStep = formRequest.querySelector(
        ".steps .step:last-child"
    );
    const requestTypeSelector = formRequest.querySelector(
        "#form-request-service__type"
    );

    // Events
    personSelect.addEventListener("change", (event) => {
        personSelect.classList.remove("is-invalid");
        let element = event.target;
        let person = element.value;
        if (person === "legal") {
            containerBusinessName.classList.add(
                "container-business-name--show"
            );
        } else {
            containerBusinessName.classList.remove(
                "container-business-name--show"
            );
        }
    });

    btnNextStep.addEventListener("click", (event) => {
        event.preventDefault();
        const inputs = [nameInput, telephoneInput, personSelect];

        if (inputs.some((input) => !input.value)) {
            inputs.forEach((input) => {
                if (!input.value) {
                    input.classList.add("is-invalid");
                } else {
                    input.classList.remove("is-invalid");
                }
            });
            return null;
        }

        if (personSelect.value === "legal" && !businessNameInput.value) {
            businessNameInput.classList.add("is-invalid");
            return null;
        }
        containerFirstStep.classList.remove("active");
        containerSecondStep.classList.add("active");
    });

    formRequest.addEventListener("submit", async (e) => {
        try {
            e.preventDefault();
            const type = requestTypeSelector.value;
            const cilinderInputs = document.querySelectorAll(
                ".form-request-service__inputs--cilinder"
            );

            const data = validateInputs(cilinderInputs);
            console.log("data ", data);

            if (data) {
                const token = formRequest.querySelector(
                    "#form-request-service__token"
                ).value;
                const formData = new FormData();
                const dataKeys = Object.keys(data);

                formData.append("_token", token);
                dataKeys.forEach((key) => {
                    formData.append(key, data[key]);
                });

                const response = await fetch("/request-service", {
                    method: "POST",
                    body: formData,
                });

                if (response.status === 200) {
                    const popupModal = new bootstrap.Modal(
                        document.getElementById("popup"),
                        {}
                    );
                    popupModal.show();

                    // Reset fields
                    const allInputs =
                        formRequest.querySelectorAll("input, select");
                    allInputs.forEach((input) => {
                        input.classList.remove("is-invalid");
                    });
                    formRequest.reset();
                    containerFirstStep.classList.add("active");
                    containerSecondStep.classList.remove("active");
                } else {
                    alert("There is a problem trying to send the email.");
                }
            }
        } catch (e) {
            console.log(e);
        }
    });

    // Functions
    const validateInputs = (inputs) => {
        const selectedInputs = Array.from(inputs).map((input) => {
            return input.querySelectorAll("input, select")[0];
        });
        if (selectedInputs.some((input) => !input.value)) {
            selectedInputs.forEach((input) => {
                if (!input.value) {
                    input.classList.add("is-invalid");
                } else {
                    input.classList.remove("is-invalid");
                }
            });
            return false;
        }
        const data = {};

        selectedInputs.forEach(
            (input) => (data[input.name] = input.value)
        );
        const otherInputs = [
            nameInput,
            telephoneInput,
            personSelect,
            businessNameInput,
        ];
        otherInputs.forEach((input) => {
            if (input.value) {
                data[input.name] = input.value;
            }
        });
        return data;
    };
});
