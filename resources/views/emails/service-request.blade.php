<table>
    <tr>
        <td>Name: </td>
        <td>{{ $data['name'] }}</td>
    </tr>
    <tr>
        <td>Telephone: </td>
        <td>{{ $data['telephone'] }}</td>
    </tr>
    <tr>
        <td>Type of person: </td>
        <td>{{ $data['person'] }}</td>
    </tr>

    @if(isset($data['business-name']))
    <tr>
        <td>Business name: </td>
        <td>{{ $data['business-name'] }}</td>
    </tr>
    @endif

    <tr>
        <td>Space: </td>
        <td>{{ $data['space'] }}</td>
    </tr>
    <tr>
        <td>Quanity: </td>
        <td>{{ $data['quantity'] }}</td>
    </tr>
    <tr>
        <td>Budget: </td>
        <td>{{ $data['budget'] }}</td>
    </tr>
</table>
