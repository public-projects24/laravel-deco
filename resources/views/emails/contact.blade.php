<table>
    <tr>
        <td>Name: </td>
        <td>{{ $data['name'] }}</td>
    </tr>
    <tr>
        <td>ID: </td>
        <td>{{ $data['document'] }}</td>
    </tr>
    <tr>
        <td>City: </td>
        <td>{{ $data['city'] }}</td>
    </tr>
     <tr>
        <td>Phone number: </td>
        <td>{{ $data['phone'] }}</td>
    </tr>
    <tr>
        <td>Email: </td>
        <td>{{ $data['email'] }}</td>
    </tr>
    <tr>
        <td>Description: </td>
        <td>{{ $data['description'] }}</td>
    </tr>
</table>
