@extends('admin.layouts.master')
@section('content')

<div class="p-5 mt-4 mb-4 bg-light rounded-3">
  <div class="container-fluid py-5">
    <h1 class="display-5 fw-bold">¡Welcome!</h1>
    <p class="col-md-8 fs-4">This is the {{ config('app.name') }} admin dashboard, here you
        can edit all available sections.</p>
  </div>
</div>
@endsection
