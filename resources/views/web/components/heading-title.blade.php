<div class="heading-title">
    <h2>{{ $title }}</h2>
    @isset($description)
    <p>{!! $description !!}</p>
    @endisset
</div>
