<!-- Request Service -->
<section class="section">
    <div class="container">
        <div class="row  d-flex justify-content-end">
            <div class="request-service">
                <div class="image">
                    <img
                        src="{{ asset('images/web/home/contact.jpg') }}"
                        class="img-fluid"
                        width="350"
                        alt="">
                </div>
                <div class="content">
                    <div class="title">
                        <h4>Request service</h4>
                        <h5>We are here for you</h5>
                    </div>
                    <form id="form-request-service" class="form__general">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"
                               id="form-request-service__token">
                        <div class=" steps">
                            <div class="step active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Name</label>
                                        <input type="text" autocomplete="off" name="name"
                                               class="form-control"
                                               placeholder="Name" aria-label="Name">
                                        <div class="invalid-feedback">
                                            Field required
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Phone number</label>
                                        <input type="number" autocomplete="off" name="telephone"
                                               class="form-control"
                                               placeholder="Phone number" aria-label="Phone number">
                                        <div class="invalid-feedback">
                                            Field required
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Type of person</label>
                                        <div class="select">
                                            <select name="person" class="form-control">
                                                <option value="">Type of person</option>
                                                <option value="natural">Natural</option>
                                                <option value="legal">Legal</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                Field required
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6  container-business-name">
                                        <label for="">Business name</label>
                                        <input type="text" autocomplete="off" name="business-name"
                                               class="form-control"
                                               placeholder="Business name"
                                               aria-label="Business name">
                                        <div class="invalid-feedback">
                                            Field required
                                        </div>
                                    </div>
                                </div>
                                <div class="row controls">
                                    <div class="col-md-12">
                                        <div class="buttons">
                                            <button class="btn btn-primary" id="btn-next-step">
                                                Next
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="step">
                                <div class="row">
                                    <div
                                        class="col-md-6 form-request-service__inputs--cilinder show">
                                        <label for="">Space</label>
                                        <div class="select">
                                            <select name="space" id="form-request-service__type"
                                                    class="form-control">
                                                <option value="residential">Residential</option>
                                                <option value="office">Office</option>
                                                <option value="commercial">Commercial</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                Field required
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="col-md-6 form-request-service__inputs--cilinder show">
                                        <label for="">Number of spaces</label>
                                        <input type="number" name="quantity" class="form-control"
                                               placeholder="Number of spaces">
                                        <div class="invalid-feedback">
                                            Field required
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div
                                        class="col-md-6 form-request-service__inputs--cilinder show">
                                        <label for="">Budget</label>
                                        <div class="select">
                                            <select name="budget" id="" class="form-control">
                                                <option value="$1.000 - $5.000">$1.000 - $5.000
                                                </option>
                                                <option value="$5.000 - $100.000">$5.000 -
                                                    $100.000
                                                </option>
                                                <option value="$100.000 - $500.000">$100.000 -
                                                    $500.000
                                                </option>
                                                <option value="$500.000+">$500 .000+</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                Field required
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row controls">
                                    <div class="col-md-12">
                                        <div class="buttons">
                                            <button type="submit"
                                                    class="btn btn-primary request-service__button">
                                                Send Request
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row indicators">
                            <div class="col-lg-3">
                                <div class="dots">
                                    <span class="dots-item active"></span>
                                    <span class="dots-item "></span>
                                </div>
                            </div>
                            <div class="col-lg-9">

                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- End Request Service -->
