<div class="modal fade" id="popup" tabindex="-1" aria-labelledby="popUpLabel" aria-hidden="true">
    <div class="modal-dialog .modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                @component('web.components.heading-title')
                @slot('title')Thank you, your request has been sent @endslot
                @endcomponent
                <p>We will contact you soon</p>
                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
