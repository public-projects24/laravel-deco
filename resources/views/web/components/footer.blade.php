<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <img src="{{ asset('images/web/common/logo.svg') }}" alt="logo"
                     class="img-fluid">
            </div>
            <div class="col-12 col-md-3">
                <ul class="footer__sitemap">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('web.home') }}"><i
                                class="icon-cheveron-right"></i>
                            Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('web.blog') }}"><i
                                class="icon-cheveron-right"></i>News</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('web.pqrs') }}"><i
                                class="icon-cheveron-right"></i>
                            Contact us</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-3 footer__contact">
                <h3>
                    Contact
                </h3>
                <hr>
                <h4>
                    Phone Number:
                </h4>
                <p>
                    (208) 333 9296
                </p>
                <h4>
                    Email:
                </h4>
                <p>
                    info@laradeco.com
                </p>
                <h4>
                    Address:
                </h4>
                <p>
                    Duval County, Jacksonville, FL 32216
                </p>
            </div>
            <div class="col-12 col-md-3 footer__normative">
                <h3>Alliances</h3>
                <hr>
                <ul class="footer__normative-list">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page"
                            href="#">Archi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page"
                            href="#">Global design </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Eco deco</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Vibrant</a>
                    </li>
                </ul>
            </div>
        </div>
        @if(isset($upButton))
        <button class="footer__button btn btn-primary" id="footer__button"> Go up <i
                class="icon-circle-up"></i></button>
        @endif
    </div>
    <div class="footer__legal">
        <p>Created by Sergio Aristizábal</p>
    </div>
</footer>
