@extends('web.layouts.master')
@section('metatags_facebook')
    <meta property="og:title"
          content="{{ config('app.name') }} | Contact us">
    <meta property="og:site_name" content="{{ config('app.name') }}">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:description"
          content="Complete the fields and our team will contact you.">
    <meta property="og:type" content="website">
@endsection
@section('metatags_seo')
    <meta name="title"
          content="{{ config('app.name') }} | Contact us">
    <meta name="description"
          content="Complete the fields and our team will contact you.">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="30 days">
    <meta name="author" content="{{ config('app.name') }}">
@endsection
@section('title', config('app.name') . ' | Contact us')

@section('content')
    <section class="pqrs">
        <div class="container">
            <div class="row align-items-center pqrs__container">
                <div class="col-12  col-lg-5 pqrs__side-banner">
                    <div class="pqrs__side-banner-text">

                        <h3>We are here <br> to hear you.</h3>
                        <p>Ut tincidunt tristique nulla eu tempor. Cras eu faucibus felis. Aliquam
                            at sem lacus. Duis ipsum nisi, facilisis et quam a, sollicitudin
                            suscipit mauris. Curabitur tristique ex enim, sed fringilla nisi semper
                            ut. </p>
                    </div>
                    <div class="pqrs__side-banner-image">
                        <img src="{{ asset('images/web/pqrs/pqrs_img_principal.png') }}"
                             alt="Banner image"
                             class="w-100">
                    </div>
                </div>
                <div class="col-12 col-lg-7">
                    <form class="form__general pqrs__form row g-3 needs-validation"
                          novalidate id='pqrs__form'>
                        @csrf
                        <div class="form__general-header line col-12 p-0">
                            <h3>Contact us</h3>
                            <p>Lorem ipsum dolor.</p>
                        </div>
                        <div class="col-md-6 mt-0">
                            <label for="name" class="form-label">Name</label>
                            <input name="name" type="text"
                                   class="form-control" id="name"
                                   aria-label="Nombre" required>
                            <div class="invalid-feedback">
                                Required field
                            </div>
                        </div>
                        <div class="col-md-6 mt-0">
                            <label for="document" class="form-label">ID</label>
                            <input type="number" class="form-control"
                                   id="document" required name='document'>
                            <div class="invalid-feedback">
                                Required field
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="city" class="form-label">City</label>
                            <input type="text" class="form-control"
                                   id="city" required name="city">
                            <div class="invalid-feedback">
                                Required field
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="phone"
                                   class="form-label">Phone number</label>
                            <input type="number" class="form-control"
                                   id="phone" required name='phone'
                                   minlength=7>
                            <div class="invalid-feedback">
                                Required field
                            </div>
                        </div>
                        <div class="col-12">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control"
                                   id="email" required name='email'>
                            <div class="invalid-feedback">
                                Required field
                            </div>
                        </div>
                        <div class="col-12">
                            <label for="description"
                                   class="form-label">Description</label>
                            <textarea class="form-control" rows="3" required
                                      name='description'></textarea>
                            <div class="invalid-feedback">
                                Required field
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary form__general-button"
                                    type="submit">Send
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>

@endsection
