@extends('web.layouts.master')
@section('metatags_facebook')
    <meta property="og:title"
          content="{{ config('app.name') }} | Interior Design">
    <meta property="og:site_name" content="{{ config('app.name') }}">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:description"
          content="We have a wide portfolio for your residential, office and commercial projects.">
    <meta property="og:type" content="website">
    <meta property="og:image" content="{{ $homeBanner->image_url }}">
@endsection
@section('metatags_seo')
    <meta name="title" content="{{ config('app.name') }} | Interior Design">
    <meta name="description"
          content="We have a wide portfolio for your residential, office and commercial projects.">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="30 days">
    <meta name="author" content="{{ config('app.name') }}">
@endsection
@section('title', config('app.name') . ' | Interior Design')

@section('content')

    @component('web.components.banner',['logo'=> true])
        @slot('id')
            banner-home
        @endslot
        @isset($homeBanner->image)
            @slot('image')
                {{ $homeBanner->image_url }}
            @endslot
        @endisset
        @slot('title')
            {{ $homeBanner->title }}
        @endslot
        @slot('description')
            {{ $homeBanner->description }}
        @endslot
        @slot('buttonLink')
            {{ $homeBanner->button_link }}
        @endslot
        @slot('buttonText')
            {{ $homeBanner->button_text }}
        @endslot
    @endcomponent

    <!-- Team -->
    <div class="container">
        <section class="section">
            @component('web.components.heading-title')
                @slot('title')
                    {{ $homeFeature->title }}
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-md-6 d-flex align-items-center">
                    {!! $homeFeature->description !!}
                </div>
                <div class="col-md-6">
                    <div id="home-main-image">
                        @if (isset($homeFeature->image))
                            <img src="{{ $homeFeature->image_url }}" class="img-fluid"
                                 alt="Team">
                        @else
                            <img src="{{ asset('images/web/home/team.jpg') }}" class="img-fluid"
                                 alt="Team">
                        @endif
                        <div class="content">
                            <p>{{ $homeFeature->image_description }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- End Team -->

    <hr class="section-bottom-separator">

    <!-- Services -->
    <div class="container">
        <section class="section">
            @component('web.components.heading-title')
                @slot('title')
                    Ideas for your projects
                @endslot
                @slot('description')
                    Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                    in
                    reprehenderitn.
                @endslot
            @endcomponent

            <div class="tabs">
                <div class="tabs-menu">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="products-1" data-bs-toggle="tab"
                                    data-bs-target="#products-content-1" type="button" role="tab"
                                    aria-controls="products-content-1"
                                    aria-selected="true">Residential
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="products-2" data-bs-toggle="tab"
                                    data-bs-target="#products-content-2" type="button" role="tab"
                                    aria-controls="products-content-2"
                                    aria-selected="false">Hospitality
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="products-3" data-bs-toggle="tab"
                                    data-bs-target="#products-content-3" type="button" role="tab"
                                    aria-controls="products-content-3" aria-selected="false">Office
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="products-4" data-bs-toggle="tab"
                                    data-bs-target="#products-content-4" type="button" role="tab"
                                    aria-controls="products-content-4"
                                    aria-selected="false">Commercial
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="tab-content tabs-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="products-content-1" role="tabpanel"
                         aria-labelledby="products-1">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-01.jpg') }}"
                                     class="w-100"
                                     alt="Luxury living room">
                                <h3>Luxury living room</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-02.jpg') }}"
                                     class="w-100"
                                     alt="Modern bedroom">
                                <h3>Modern bedroom</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-03.jpg') }}"
                                     class="w-100"
                                     alt="Summer house">
                                <h3>Summer house</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="products-content-2" role="tabpanel"
                         aria-labelledby="products-2">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-04.jpg') }}"
                                     class="w-100"
                                     alt="Modern elegance suite">
                                <h3>Modern elegance suite</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-05.jpg') }}"
                                     class="w-100"
                                     alt="Apartment renovation">
                                <h3>Apartment renovation</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-06.jpg') }}"
                                     class="w-100"
                                     alt="Cozy bedroom">
                                <h3>Cozy bedroom</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="products-content-3" role="tabpanel"
                         aria-labelledby="products-3">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-07.jpg') }}"
                                     class="w-100"
                                     alt="Modern office">
                                <h3>Modern office</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>

                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-08.jpg') }}"
                                     class="w-100"
                                     alt="Office on space">
                                <h3>Office on space</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-09.jpg') }}"
                                     class="w-100"
                                     alt="Eco green office">
                                <h3>Eco green office</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="products-content-4" role="tabpanel"
                         aria-labelledby="products-4">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-10.jpg') }}"
                                     class="w-100"
                                     alt="Restaurant in texas">
                                <h3>Restaurant in texas</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-11.jpg') }}"
                                     class="w-100"
                                     alt="Restaurant in Cannes">
                                <h3>Restaurant in Cannes</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('images/web/home/services/serv-10.jpg') }}"
                                     class="w-100"
                                     alt="Restaurant in Italy">
                                <h3>Restaurant in Italy</h3>
                                <p>Proin convallis nibh ac velit convallis bibendum. Fusce aliquet
                                    elit a magna gravida, vel volutpat justo laoreet. Etiam ornare
                                    ex eu vehicula rutrum. Class aptent taciti sociosqu ad litora
                                    torquent per conubia nostra, per inceptos himenaeos. </p>

                                <p>Cras varius ornare consequat. Pellentesque pharetra suscipit ante
                                    a imperdiet. </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
    <!-- End Services -->
    @include('web.components.request-service')
    @include('web.components.blog-highlights')
@endsection
