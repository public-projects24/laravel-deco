@extends('web.layouts.secondary')
@section('content')
    <section class="thanks">
        <img src="{{ asset('images\web\thanks\thankyou_img_ppl.png') }}" alt="thank you"
             class="w-100">
        <div class="thanks__content container">
            <div class="thanks__content-text">
                <h1>¡Thank you for contacting us!</h1>
                <p>Aenean orci tellus, ultricies ac lectus eget, sodales laoreet leo.</p>
                <a class='btn btn-primary--dark ' href="{{ route('web.home') }}">Go back
                    home</a>
            </div>
        </div>
    </section>
@endsection
