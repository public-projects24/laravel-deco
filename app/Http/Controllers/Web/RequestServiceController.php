<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Home\HomeBanner;
use App\Models\Home\HomeFeature;
use App\Models\Home\HomeRate;
use Illuminate\Support\Facades\Mail;
use App\Mail\ServiceRequestMail;
use App\Http\Requests\Web\ServiceRequest;

class RequestServiceController extends Controller
{
    public function index(ServiceRequest $request)
    {
        try {
            $contacts = ['info@laraveldeco.com'];
            Mail::to($contacts)->send(new ServiceRequestMail($request->all()));
            return response()->json([
                'status' => 200,
                'message' => 'Message sent'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'message' => 'Error sending message',
                'description' => $e->getMessage()
            ], 500);
        }
    }
}
