<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Home\HomeBanner;
use App\Models\Home\HomeFeature;
use App\Models\Home\HomeRate;
use Illuminate\Support\Facades\Mail;
use App\Mail\ServiceRequestMail;
use App\Http\Requests\Web\ServiceRequest;

class HomeController extends Controller
{
    public function index()
    {
        $homeBanner = HomeBanner::first();
        $homeFeature = HomeFeature::first();
        return view('web.home', compact('homeBanner', 'homeFeature'));
    }
}
