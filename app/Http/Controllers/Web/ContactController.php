<?php

namespace App\Http\Controllers\Web;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Models\Pqrs\PqrsObject;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\Web\ContactRequest;

class ContactController extends Controller
{
    public function index()
    {
        return view('web.contact');
    }

    public function store(ContactRequest $request)
    {
        try {
            $contacts = ['customerservice@laraveldeco.com'];
            Mail::to($contacts)->send(new ContactMail($request->all()));
            return response()->json([
                'status' => 200,
                'message' => 'Message sent'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'message' => 'Error sending message',
                'description' => $e->getMessage()
            ], 500);
        }
    }

    public function thanks()
    {
        return view('web.thank-you-page');
    }
}
