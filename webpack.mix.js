const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Admin
 |--------------------------------------------------------------------------
 |
 */

mix.js('resources/js/admin/app.js', 'public/js/admin')
    .sass('resources/scss/admin/app.scss', 'public/css/admin').options({
        processCssUrls: false
    }).version();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Rayogas
 |--------------------------------------------------------------------------
 |
 */

mix.js('resources/js/web/app.js', 'public/js/web')
    .sass('resources/scss/web/app.scss', 'public/css/web').options({
        processCssUrls: false
    }).version();
