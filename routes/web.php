<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/
Route::prefix('admin')->group(function () {
    Route::namespace("App\Http\Controllers\Admin")->group(function () {
        // Authentication
        Route::middleware(['GuestAdminUser'])->group(function () {
            Route::view('login', 'admin.auth.login')->name('admin.login');
        });

        Route::post('login', 'LoginController@index')
        ->name('admin.auth.login');

        Route::post('logout', 'LogoutController@index')
        ->name('admin.auth.logout');

        // Por hacer: Verificar que solo puedan ingresar usuarios de tipo administrador
        Route::middleware(['CheckAdminUserAuth'])->group(function () {
            Route::view('/', 'admin.welcome')->name('admin.welcome');

            // Home
            Route::resource('/home/banner', 'Home\HomeBannerController', ['as' => 'admin.home'])->only(['edit', 'update']);

            Route::resource('/home/feature', 'Home\HomeFeatureController', ['as' => 'admin.home'])->only(['edit', 'update']);

            Route::resource('/home/rates', 'Home\HomeRateController', ['as' => 'admin.home']);

            // Blog
            Route::resource('/blog/banner', 'Blog\BlogBannerController', ['as' => 'admin.blog'])->only(['edit', 'update']);

            Route::resource('/blog/posts', 'Blog\BlogPostController', ['as' => 'admin.blog']);
        });
    });
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::namespace("App\Http\Controllers\Web")->group(function () {
    Route::get("/", "HomeController@index")->name('web.home');
    Route::post("/request-service", "RequestServiceController@index");

    Route::get("/news", "BlogController@index")->name('web.blog');
     Route::get("/news/{slug}", "BlogController@show")->name('web.blog.show');
    Route::get("/contact", "ContactController@index")->name('web.pqrs');
    Route::post("/contact", "ContactController@store");
    Route::get("contact/thanks", "ContactController@thanks")->name('web.thanks');
});
