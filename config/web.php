<?php

return [
    'uploadsFolder' => env('UPLOADS_FOLDER', null),
    'home' => [
        'banner' => env('HOME_BANNER_FOLDER', null),
        'feature' => env('HOME_FEATURE_FOLDER', null),
    ],
    'blog' => [
        'banner' => env('BLOG_BANNER_FOLDER', null),
        'posts' => env('BLOG_POSTS_FOLDER', null),
    ],
];
