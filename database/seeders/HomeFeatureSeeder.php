<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HomeFeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Home\HomeFeature::factory(1)->create([
            'title' => 'Our team',
            'description' => '<p>Sed ut perspiciatis <strong>unde omnis</strong> iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore. Sed ut perspiciatis unde omnis iste natus error sit <strong>voluptatem accusantium</strong> doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>',
            'image_description' => 'Class aptent taciti sociosqu ad litora torquent per conubia totam.',
        ]);
    }
}
