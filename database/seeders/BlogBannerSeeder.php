<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BlogBannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Blog\BlogBanner::create([
            'title' => 'Catch up with our news',
            'description' => 'Proin convallis nibh ac velit convallis bibendum. Fusce aliquet elit a magna gravida, vel volutpat justo laoreet. Etiam ornare ex eu vehicula rutrum.',
        ]);
    }
}
