<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HomeBannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Home\HomeBanner::factory(1)->create([
            'title' => 'Interior Design',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.',
            'button_text' => 'Contact us',
            'button_link' => '/contact'
        ]);
    }
}
