<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        \App\Models\User::factory(1)->create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
        ]);

        /*
        |----------------------------------------------------------------------
        | Home seeders
        |----------------------------------------------------------------------
        */
        $this->call(HomeBannerSeeder::class);
        $this->call(HomeFeatureSeeder::class);

        /*
        |----------------------------------------------------------------------
        | Blog seeders
        |----------------------------------------------------------------------
        */
        $this->call(BlogBannerSeeder::class);
        $this->call(BlogPostSeeder::class);
    }
}
